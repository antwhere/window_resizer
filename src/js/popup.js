﻿window.addEventListener('DOMContentLoaded', (e) => {
    info();

    var list = getList(storageName);
    var listStr = '';
    list.forEach(function (obj) {
        var str =
            `<label class="switch-group" data-width="${obj.width}" data-height="${obj.height}">
                <div class="switch">
                    <span class="label">${formatItem(obj)}</span>
                </div>
            </label>`;
        listStr += str;
    });
    document.querySelector('#list').innerHTML = listStr;
    document.querySelector('body').style.display = 'block';

    document.querySelectorAll('.switch-group').forEach(function (ele) {
        ele.addEventListener('click', function () {
            var w = parseInt(this.getAttribute('data-width'), 10);
            var h = parseInt(this.getAttribute('data-height'), 10);
            chrome.windows.update(chrome.windows.WINDOW_ID_CURRENT, {
                width: w,
                height: h,
                state: 'normal'
            }, function () {
                window.close();
            });
        });
    });

    document.querySelector('#options').addEventListener('click', function () {
        if (chrome.runtime.openOptionsPage) {
            chrome.runtime.openOptionsPage();
        } else {
            window.open(chrome.runtime.getURL('options.html'));
        }
        window.close();
    });
});

function info() {
    chrome.windows.get(chrome.windows.WINDOW_ID_CURRENT, function (win) {
        document.querySelector('#window').innerHTML = win.width + 'x' + win.height;
    });
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        document.querySelector('#viewport').innerHTML = tabs[0].width + 'x' + tabs[0].height;
    });
}