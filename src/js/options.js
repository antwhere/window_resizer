document.querySelector('#add').addEventListener('click', function () {
    var name = document.querySelector('#name').value;
    var width = document.querySelector('#width').value;
    var height = document.querySelector('#height').value;
    if (width && height) {
        width = parseInt(width, 10);
        height = parseInt(height, 10);
        width = width <= 0 ? 1 : width;
        height = height <= 0 ? 1 : height;
        var list = getList(storageName);
        list.push({
            name: name,
            width: width,
            height: height
        });
        setList(storageName, list);
        reloadPage();
    } else {
        alert('Please fill in the width and height!');
    }
});

var listEle = document.querySelector('#list');
var listStr = '';
getList(storageName).forEach(function (obj, index) {
    listStr += '<div class="item"><button class="delete" data-index="' + index + '">delete</button> <button class="up" data-index="' + index + '">up</button> <button class="down" data-index="' + index + '">down</button> <button class="edit" data-index="' + index + '">edit</button> ' + formatItem(obj) + '</div>';
});
listEle.innerHTML = listStr;
document.querySelectorAll('.delete').forEach(function (ele) {
    ele.addEventListener('click', function () {
        var index = parseInt(this.getAttribute('data-index'), 10);
        var item = getList(storageName)[index];
        if (confirm('Confirm to delete "' + formatItem(item) + '" ?')) {
            deleteList(storageName, index);
            reloadPage();
        }
    });
});
document.querySelectorAll('.up').forEach(function (ele) {
    ele.addEventListener('click', function () {
        var index = parseInt(this.getAttribute('data-index'), 10);
        moveList(storageName, index, index - 1);
        reloadPage();
    });
});
document.querySelectorAll('.down').forEach(function (ele) {
    ele.addEventListener('click', function () {
        var index = parseInt(this.getAttribute('data-index'), 10);
        moveList(storageName, index, index + 1);
        reloadPage();
    });
});
document.querySelectorAll('.edit').forEach(function (ele) {
    ele.addEventListener('click', function () {
        var update = document.querySelector('#update');
        update.querySelectorAll('[disabled]').forEach(function (ele) {
            ele.removeAttribute('disabled');
        });
        var index = parseInt(this.getAttribute('data-index'), 10);
        var item = getList(storageName)[index];
        var indexEle = update.querySelector('.index');
        var nameEle = update.querySelector('.name');
        var widthEle = update.querySelector('.width');
        var heightEle = update.querySelector('.height');
        var saveEle = update.querySelector('.save');
        indexEle.value = index;
        nameEle.value = item.name;
        widthEle.value = item.width;
        heightEle.value = item.height;
        saveEle.addEventListener('click', function () {
            var index = parseInt(indexEle.value, 10);
            var name = nameEle.value;
            var width = widthEle.value;
            var height = heightEle.value;
            if (width && height) {
                width = parseInt(width, 10);
                height = parseInt(height, 10);
                width = width <= 0 ? 1 : width;
                height = height <= 0 ? 1 : height;
                var list = getList(storageName);
                list[index] = {
                    name: name,
                    width: width,
                    height: height
                };
                setList(storageName, list);
                reloadPage();
            } else {
                alert('Please fill in the width and height!');
            }
        });
    });
});
(function () { // Reset, Import/Export Settings
    var resetBtn = document.getElementById('reset');
    var exportBtn = document.getElementById('export');
    var importBtn = document.getElementById('import');
    resetBtn.addEventListener('click', function () {
        if (confirm('Confirm to restore the default settings? This will delete all custom resolution items.')) {
            setList(storageName, resolution);
            reloadPage();
        }
    });
    exportBtn.addEventListener('click', function () {
        var exportObj = {
            window_resizer: getList(storageName)
        };
        downloadFile('window_resizer-' + formatDate(new Date(), 'yyyy_MM_dd_HH_mm_ss') + '.json', JSON.stringify(exportObj));
    });
    importBtn.addEventListener('click', function () {
        var fileInput = document.getElementById('fileInput');
        fileInput.addEventListener('change', function (e) {
            var file = fileInput.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                var content = reader.result;
                var obj = JSON.parse(content);
                setList(storageName, obj.window_resizer);
                alert('Import successful!');
                reloadPage();
            }
            reader.readAsText(file);
        });
        fileInput.click();
    });
})();