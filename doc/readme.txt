Resize the browser window. The resolution list supports customization.

✅ open-source: https://bitbucket.org/antwhere/window_resizer/
----------

Update history:
1.6: Migrate to Manifest V3.
1.5: Enhanced input data verification.
1.4: Add editing function.
1.3: Add rich customization options.
1.2: Fixed bugs, improved performance.
1.1: The open source Window Resizer extension is here!
----------

Privacy Policy:
We do not collect any information from the first version until now and forever!
----------

The initial version began in June 2020.
Feedback E-mail: antwhere@antwhere.com
Official Website: www.antwhere.com